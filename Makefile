##
# fancontroller
#
# @file
# @version 0.1

MCU=atmega328p
DISPLAY=

ifdef DISPLAY
	FEATURES=$(MCU),display,display-$(DISPLAY)
else
	FEATURES=$(MCU)
endif

target/avr-$(MCU)/release/fancontroller.hex: target/avr-$(MCU)/release/fancontroller.elf
	avr-objcopy -O ihex target/avr-$(MCU)/release/fancontroller.elf target/avr-$(MCU)/release/fancontroller.hex

target/avr-$(MCU)/release/fancontroller.elf: src/* Cargo.toml Cargo.lock
	cargo build --release --target .cargo/avr-$(MCU).json --features $(FEATURES)

.PHONY: doc
doc:
	cargo doc --target .cargo/avr-$(MCU).json --features $(FEATURES)


##
# flash

.PHONY: flash
flash: target/avr-$(MCU)/release/fancontroller.hex
ifeq ($(MCU),atmega328p)
	avrdude -c USBasp -p m328p -U flash:w:target/avr-$(MCU)/release/fancontroller.hex
else
	dfu-programmer $(MCU) erase
	dfu-programmer $(MCU) flash target/avr-$(MCU)/release/fancontroller.hex
	dfu-programmer $(MCU) launch
endif

.PHONY: clean
clean:
	rm target/avr-$(MCU)/release/fancontroller.elf target/avr-$(MCU)/release/fancontroller.hex

# end
