use embedded_hal::blocking::i2c::Write;
// use ssd1306::{mode::terminal::TerminalDisplaySize, mode::TerminalMode, prelude::I2CInterface};
use ssd1306::{mode::{TerminalMode, TerminalDisplaySize}, prelude::*, Ssd1306};

pub struct SSD1306<I2C: Write, Size: TerminalDisplaySize> {
    pub terminal: Ssd1306<I2CInterface<I2C>, Size, TerminalMode>,
}
