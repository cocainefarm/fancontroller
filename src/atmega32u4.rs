use atmega32u4_hal as hal;

pub use hal::entry;

use hal::prelude::*;

use hal::{clock, pac, port};
use hal::{adc::Adc, i2c::I2cMaster, port::PortExt};

use crate::SMOOTHING_ITERS;
use crate::Display;

pub struct Atmega32u4 {
    // dp: pac::Peripherals,
    tc4: hal::pac::TC4,
    adc: Adc,
    input: hal::port::portb::PB4<hal::port::mode::Analog>,
    #[cfg(feature = "display-ssd1306")]
    display: crate::display::ssd1306::SSD1306<
        I2cMaster<clock::MHz16, port::mode::Input<port::mode::PullUp>>,
        ssd1306::prelude::DisplaySize128x64,
    >,
}

#[cfg(feature = "display-ssd1306")]
impl crate::Display
    for crate::display::ssd1306::SSD1306<
        I2cMaster<clock::MHz16, port::mode::Input<port::mode::PullUp>>,
        ssd1306::prelude::DisplaySize128x64,
    >
{
    fn new() -> Self {
        let dp = unsafe { pac::Peripherals::steal() };

        let mut portd = dp.PORTD.split();
        let twi = dp.TWI;

        let i2c = I2cMaster::<hal::clock::MHz16, hal::i2c::I2cPullUp>::new(
            twi,
            portd.pd1.into_pull_up_input(&mut portd.ddr),
            portd.pd0.into_pull_up_input(&mut portd.ddr),
            57600,
        );

        let interface = ssd1306::I2CDIBuilder::new().init(i2c);
        let mut disp: ssd1306::prelude::TerminalMode<_, _> = ssd1306::Builder::new().connect(interface).into();
        disp.init().unwrap();
        disp.clear().unwrap();

        Self {
            terminal: disp
        }
    }

    fn write_value(&mut self, duty: u16) {
        use core::fmt::Write;
        use num_format::{Buffer, Locale};

        self.clear();
        self.terminal.write_str("Duty: ").unwrap();

        let mut buf = Buffer::default();

        buf.write_formatted(&duty, &Locale::en);
        self.terminal.write_str(buf.as_str()).unwrap();

    }

    fn clear(&mut self) {
        self.terminal.clear().unwrap();
    }
}

impl crate::Controller for Atmega32u4 {
    fn new() -> Self {
        let dp = unsafe { pac::Peripherals::steal() };

        dp.TC4.tccr4a.write(|w| unsafe { w.bits(0) });
        dp.TC4.tccr4b.write(|w| w.cs4().prescale_8());
        dp.TC4.tccr4d.write(|w| unsafe { w.bits(0) });
        dp.TC4.tccr4c.write(|w| unsafe { w.bits(0) });

        dp.PLL.pllcsr.write(|w| w.plle().set_bit());
        dp.PLL.pllfrq.write(|w| w.plltm().factor_2().pdiv().mhz48());

        dp.TC4.ocr4c.write(|w| unsafe { w.bits(255) });
        dp.TC4.ocr4d.write(|w| unsafe { w.bits(128) });
        dp.PORTD.ddrd.write(|w| w.pd7().set_bit());
        dp.TC4
            .tccr4c
            .write(|w| w.com4d().match_clear().pwm4d().set_bit());

        let portb = dp.PORTB.split();
        let mut adc = Adc::new(dp.ADC, Default::default());
        let input = portb.pb4.into_analog_input(&mut adc);

        #[cfg(feature = "display-ssd1306")]
        let display = crate::display::ssd1306::SSD1306::new();

        Self {
            tc4: dp.TC4,
            adc,
            input,
            #[cfg(feature = "display-ssd1306")]
            display,
        }
    }

    fn set_duty(&self, duty: u16) {
        self.tc4.ocr4d.write(|w| unsafe { w.bits((duty / 4) as u8) });
    }

    fn get_smoothed_value(&mut self) -> u16 {
        let mut values: [u16; SMOOTHING_ITERS] = [0; SMOOTHING_ITERS];
        for i in values.iter_mut() {
            *i = nb::block!(self.adc.read(&mut self.input)).void_unwrap();
        }

        values.iter().sum::<u16>() / SMOOTHING_ITERS as u16
    }

    #[cfg(feature = "display-ssd1306")]
    fn display_value(&mut self, duty: u16) {
        self.display.write_value((duty as u32 * 100 / 1024) as u16);
    }
}
