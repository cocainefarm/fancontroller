#![no_std]
#![no_main]

const SMOOTHING_ITERS: usize = 64;

extern crate panic_halt;

#[cfg(feature = "atmega32u4")]
mod atmega32u4;
#[cfg(feature = "atmega32u4")]
use atmega32u4::entry;

#[cfg(feature = "atmega328p")]
mod atmega328p;
#[cfg(feature = "atmega328p")]
use atmega328p::entry;

mod display;

pub trait Controller {
    fn new() -> Self;
    fn set_duty(&self, duty: u16);
    fn get_smoothed_value(&mut self) -> u16;
    #[cfg(feature = "display-ssd1306")]
    fn display_value(&mut self, duty: u16);
    fn run(&mut self) {
        let mut curr = 0;

        loop {
            let value = self.get_smoothed_value();
            if value >> 2 != curr >> 2 {
                self.set_duty(value);

                #[cfg(feature = "display-ssd1306")]
                self.display_value(value);

                curr = value;
            }
        }
    }
}

pub trait Display {
    fn new() -> Self;
    fn write_value(&mut self, duty: u16);
    fn clear(&mut self);
}

#[entry]
fn main() -> ! {
    #[cfg(feature = "atmega32u4")]
    let mut controller = atmega32u4::Atmega32u4::new();
    #[cfg(feature = "atmega328p")]
    let mut controller = atmega328p::Atmega328p::new();

    controller.run();

    loop {}
}
