use atmega328p_hal as hal;

pub use hal::entry;

use hal::prelude::*;

use hal::{adc::Adc, port::PortExt};
use hal::pac;

#[cfg(feature = "display")]
use hal::{i2c::I2cMaster, clock, port};

#[cfg(feature = "display")]
use crate::Display;
use crate::SMOOTHING_ITERS;

pub struct Atmega328p {
    tc2: hal::pac::TC2,
    adc: Adc,
    input: hal::port::portc::PC0<hal::port::mode::Analog>,
    #[cfg(feature = "display-ssd1306")]
    display: crate::display::ssd1306::SSD1306<
        I2cMaster<clock::MHz16, port::mode::Input<port::mode::PullUp>>,
        ssd1306::prelude::DisplaySize128x64,
    >,
}

#[cfg(feature = "display-ssd1306")]
impl crate::Display
    for crate::display::ssd1306::SSD1306<
        I2cMaster<clock::MHz16, port::mode::Input<port::mode::PullUp>>,
        ssd1306::prelude::DisplaySize128x64,
    >
{
    fn new() -> Self {
        use ssd1306::{prelude::*, I2CDisplayInterface, Ssd1306};

        let dp = unsafe { pac::Peripherals::steal() };

        let mut portc = dp.PORTC.split();
        let twi = dp.TWI;

        let i2c = I2cMaster::<hal::clock::MHz16, hal::i2c::I2cPullUp>::new(
            twi,
            portc.pc4.into_pull_up_input(&mut portc.ddr),
            portc.pc5.into_pull_up_input(&mut portc.ddr),
            57600,
        );

        let interface = I2CDisplayInterface::new(i2c);

        let mut display = Ssd1306::new(
            interface,
            DisplaySize128x64,
            DisplayRotation::Rotate0,
        ).into_terminal_mode();
        display.init().unwrap();
        display.clear().unwrap();

        use core::fmt::Write;
        display.write_str(unsafe { core::str::from_utf8_unchecked("this is a test".as_bytes()) }).unwrap();

        Self { terminal: display }
    }

    fn write_value(&mut self, mut duty: u16) {
        use core::fmt::Write;
        use num_format::{Buffer, Locale};

        self.terminal.clear().unwrap();

        self.terminal.write_str(unsafe { core::str::from_utf8_unchecked("Duty: ".as_bytes()) }).unwrap();

        let mut buff = [0; 6];
        let mut count = 5;
        for i in 1..=6 {
            let n: u8 = (duty % (10 * i)) as u8;
            buff[count] = 48 + n;
            count -= 1;
            duty = (duty - n as u16) / 10;
        }

        self.terminal.write_str(unsafe { core::str::from_utf8_unchecked(&buff) }).unwrap();
    }

    fn clear(&mut self) {
        self.terminal.clear().unwrap();
    }
}

impl crate::Controller for Atmega328p {
    fn new() -> Self {
        let dp = unsafe { pac::Peripherals::steal() };

        dp.TC2.tccr2a.write(|w| {
            w.com2b().match_clear();
            w.wgm2().pwm_fast()
        });

        dp.TC2.tccr2b.write(|w| {
            w.wgm22().set_bit();
            w.cs2().prescale_8()
        });

        dp.TC2.ocr2a.write(|w| unsafe { w.bits(79) });

        dp.PORTD.ddrd.write(|w| w.pd3().set_bit());

        let portc = dp.PORTC.split();
        let mut adc = Adc::new(dp.ADC, Default::default());
        let input = portc.pc0.into_analog_input(&mut adc);

        #[cfg(feature = "display-ssd1306")]
        let display = crate::display::ssd1306::SSD1306::new();

        Self {
            tc2: dp.TC2,
            adc,
            input,
            #[cfg(feature = "display-ssd1306")]
            display,
        }
    }

    fn set_duty(&self, duty: u16) {
        self.tc2
            .ocr2b
            .write(|w| unsafe { w.bits((duty / 12) as u8) });
    }

    fn get_smoothed_value(&mut self) -> u16 {
        let mut values: [u16; SMOOTHING_ITERS] = [0; SMOOTHING_ITERS];
        for i in values.iter_mut() {
            *i = nb::block!(self.adc.read(&mut self.input)).void_unwrap();
        }

        values.iter().sum::<u16>() / SMOOTHING_ITERS as u16
    }

    #[cfg(feature = "display-ssd1306")]
    fn display_value(&mut self, duty: u16) {
        self.display.write_value((duty as u32 * 100 / 1024) as u16);
    }
}
